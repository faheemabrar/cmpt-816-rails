Rails.application.routes.draw do
  get 'login', to: 'auth#login'
  post 'login', to: 'auth#login_post'
  get 'logout', to: 'auth#logout'

  resources :users
  resources :categories
  resources :items
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  root 'application#hello'
end
