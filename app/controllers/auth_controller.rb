class AuthController < ApplicationController
  def login
  end

  def login_post
    user = User.find_by username: params[:auth][:username]
    if user && user.authenticate(params[:auth][:password])
      create_session user
      redirect_to items_url
    else
      flash[:error] = 'Incorrect!'
      redirect_to login_url
    end
  end

  def logout
    if log_out?
      redirect_to login_url
    end
  end
end
