class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  include AuthHelper

  def hello
    render html:  'Go to <a href="/login">this link</a> to login, username: <b>faheem</b>, password: <b>123456</b>'.html_safe
  end
end
