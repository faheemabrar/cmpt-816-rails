class Item < ApplicationRecord
  enum size: {xs: 'XS', s: 'S', m: 'M', l: 'L', xl: 'XL'}
  belongs_to :category
  validates :name, presence: true, length: {minimum: 4}
end
