json.extract! item, :id, :sku, :name, :category_id, :size, :stock, :created_at, :updated_at
json.url item_url(item, format: :json)
