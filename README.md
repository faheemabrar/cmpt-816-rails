Introduction
============

This is the Rails implementation of my CMPT 816 project titled 'Qualitative and Quantitative Comparison of CRUD functionalities on Web and Desktop'.

Setup
=====

- Import ```ase.sql``` to your preferred MySQL server
- Open ```config/database.yml``` file and set the connection information of your MySQL server
- Ensure that ruby environment and rails gem is installed globally in your machine
- Run ```rails server``` on root directory to launch server in ```localhost:3000```
- Go to ```localhost:3000``` to view the application in action